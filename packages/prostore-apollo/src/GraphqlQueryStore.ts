import {
  IRequestStoreOptions,
  RequestStore,
  IRequestStoreParams
} from '@proscom/prostore';
import ApolloClient, { QueryOptions } from 'apollo-client';
import { DocumentNode } from 'graphql';
import { IMapData } from '@proscom/prostore';

export type ApolloQueryOptions<Vars> = Omit<
  QueryOptions<Vars>,
  'query' | 'variables'
>;

export interface IGraphqlQueryOptions<Vars, Data> extends IRequestStoreOptions<Vars, Data> {
  /** Additional Apollo query options */
  apolloOptions?: ApolloQueryOptions<Vars>;
}

export interface IGraphqlQueryStoreParams<Vars, Data>
  extends IRequestStoreParams<Vars, Data> {
  /**
   * GraphQL query definition using graphql AST from the graphql-tag packages
   *
   * (be sure to include ids wherever possible in order for Apoolo cache to work correctly)
   */
  query: DocumentNode;

  /** Apollo client to be used for fetching the data */
  client: ApolloClient<any>;

  /**
   * Optional callback to transform the query result into the request store state.
   * Use it to remove unnecessary nesting or transform the data
   */
  mapData?: IMapData<Data>;

  /** Additional Apollo query options */
  apolloOptions?: ApolloQueryOptions<Vars>;
}

/**
 * RequestStore tailored to make Apollo GraphQL requests
 */
export class GraphqlQueryStore<Vars, Data> extends RequestStore<
  Vars,
  Data,
  IGraphqlQueryOptions<Vars, Data>
> {
  query: DocumentNode;
  client: ApolloClient<any>;
  mapData: IMapData<Data>;
  apolloOptions: ApolloQueryOptions<Vars>;

  constructor({
    query,
    client,
    mapData = (x) => x,
    apolloOptions = {},
    ...requestParams
  }: IGraphqlQueryStoreParams<Vars, Data>) {
    super(requestParams);
    this.query = query;
    this.client = client;
    this.apolloOptions = apolloOptions;
    this.mapData = mapData;

    console.assert(
      query,
      'Parameter "query" is required for GraphqlQueryStore'
    );
    console.assert(
      client,
      'Parameter "client" is required for GraphqlQueryStore'
    );
  }

  /**
   * Performs actual GraphQL request using the Apollo Client
   *
   * @param variables - query variables
   * @param options - additional query options
   */
  async performRequest(
    variables: Vars,
    options: Partial<IGraphqlQueryOptions<Vars, Data>>
  ): Promise<Data> {
    const result = await this.client.query({
      query: this.query,
      variables,
      ...this.apolloOptions,
      ...options.apolloOptions
    });
    return this.mapData(result.data);
  }
}
