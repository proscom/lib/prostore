# `prostore-apollo`

Эта библиотека расширяет [`prostore`](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore),
добавляя в него специальные классы сторов для GraphQL-запросов,
выполняемых с помощью [Apollo Client](https://www.apollographql.com/docs/react/).

В библиотеку входят два класса: `GraphqlQueryStore` и `GraphqlWatchQueryStore`
которые расширяют `RequestStore`, обеспечивая единообразное API
для выполнения запросов и хранения их результатов в сторе,
на который можно подписаться. Первый класс использует метод `client.query`
для выполнения запросов, а второй - `client.watchQuery`.

Таким образом, `GraphqlWatchQueryStore` обновляется не только при
первичном выполнении запроса, но и в случае последующего обновления
локального кеша Apollo (например при рефетче, либо очистке кеша).

## Использование

У `GraphqlWatchQueryStore` точно такое же API, поэтому ниже
пример только для `GraphqlQueryStore`.

```javascript
import gql from 'graphql-tag';
import { GraphqlQueryStore } from '@proscom/prostore-apollo';
import { ApolloClient } from 'apollo-client';

// Создаем клиент Apollo для выполнения запросов
const client = new ApolloClient(/*...*/);

// Задаем graphql-запрос с переменными
const query = gql`
  query getData($variable: Int) {
    data(filter: $variable) {
      # Всегда включайте id в запрос, если такое поле есть
      # в типе, потому что иначе Apollo Cache сломается
      # если какой-то другой запрос вернет тот же самый объект,
      # и в нем будет id
      id
      value1
      value2
    }
  }
`;

// Создаем наш стор
const store = new GraphqlQueryStore({
  // Выполняемый запрос
  query,

  // Клиент, который будет выполнять запрос
  client,

  // Дополнительные параметры, которые будут переданы
  // в client.query
  apolloOptions: {},

  // Функция, которая определяет что положить в state.data
  // Можно положить только часть ответа, например достать вложенный объект
  // Удобно для запросов, которые выполняют только один query
  mapData: (result) => result.data,

  // Так как GraphqlQueryStore расширяет RequestStore
  // то можно передать все параметры конструктора RequestStore
  // см. подробнее в Readme @proscom/prostore
  // https://gitlab.com/proscom/prostore/-/blob/master/packages/prostore/README.md#requeststore

  // Первоначальное значение
  initialData: null,

  // Функция пропуска запроса в зависимости от переменных
  skipQuery: (vars) => undefined,

  // Функция обновления данных при повторном запросе
  updateData: (data, oldData, params) => data,

  // Уникальный идентификатор стора для передачи данных
  // при использовании серверного рендеринга
  ssrId: undefined
});

// Чтобы инициировать запрос данных, надо вызвать
store.loadData(
  // variables - переменные запроса, определенные в query
  {
    variable: 5
  },
  // options - объект опций
  {
    // можно перезаписать состояние стора вручную
    changeState: {},
    // можно передать какие-то параметры в apollo.query
    apolloOptions: {}
  }
);

// Можно подождать ответа сразу
try {
  const result = await store.loadData(/* ... */);
} catch (error) {
  // ...
}

// А можно подписаться на все изменения стора
store.state$.subscribe((state) => {
  const { loading, loaded, error, data, variables } = state;
  // ...
});
```

Для реализации бесконечной подгрузки, см. раздел
["Пагинация с дозагрузкой"](https://gitlab.com/proscom/prostore/-/blob/master/packages/prostore/README.md#%D0%BF%D0%B0%D0%B3%D0%B8%D0%BD%D0%B0%D1%86%D0%B8%D1%8F-%D1%81-%D0%B4%D0%BE%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%BE%D0%B9)
документации `@proscom/prostore`.
