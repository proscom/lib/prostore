import { DependencyList, useContext, useMemo } from 'react';
import { PartialKey } from '@proscom/prostore';
import {
  GraphqlWatchQueryStore,
  IGraphqlWatchQueryOptions,
  IGraphqlWatchQueryStoreParams
} from '@proscom/prostore-apollo';
import {
  ProstoreSsrContext,
  UseRequestOptions,
  useRequestStore
} from '@proscom/prostore-react';
import { useContextApolloClient } from './ApolloClientsContext';
import { ApolloClient } from 'apollo-client';

export type IUseGraphqlWatchQueryOptions<Vars, Data> = PartialKey<
  IGraphqlWatchQueryStoreParams<Vars, Data>,
  'client'
>;

export interface IUseGraphqlWatchQueryParams<Vars, Data> {
  /** Apollo Client instance to be used for data fetching, or its name for it to be injected from the context */
  client?: string | ApolloClient<any>;
  /** Query specification */
  queryOptions: IUseGraphqlWatchQueryOptions<Vars, Data>;
  /** Query variables */
  variables?: Vars;
  /** Additional query options */
  options?: IGraphqlWatchQueryOptions<Vars, Data>;
  /** Additional hook options */
  hookOptions?: UseRequestOptions<Vars, Data>;
}

/**
 * Creates GraphqlWatchQueryStore and subscribes to it using useRequestStore.
 * Effectively this requests data from the apollo based on the provided variables and queryOptions
 *
 * @returns {{load, check, state}} - Current request state
 */
export function useGraphqlWatchQuery<Vars, Data>({
  client,
  queryOptions,
  variables,
  options,
  hookOptions
}: IUseGraphqlWatchQueryParams<Vars, Data>) {
  const actualClient = useContextApolloClient(client);
  const ssrContext = useContext(ProstoreSsrContext);

  const store = useMemo(
    () =>
      new GraphqlWatchQueryStore({
        ssrContext,
        client: actualClient,
        ...queryOptions
      }),
    [ssrContext, actualClient, queryOptions]
  );

  return useRequestStore(store, variables, options, hookOptions);
}

export interface IUseGraphWatchQueryVarsParams<Vars, Data>
  extends IUseGraphqlWatchQueryParams<Vars, Data> {
  variableCreator: [() => Vars, DependencyList];
}

/**
 * The same as useGraphqlWatchQuery, but it uses variableCreator which is supplied to useMemo and
 * can be used to memoize query variables. This may be used when the default behavior of deeply
 * comparing variables is not preferable (i.e., it is not fast enough, or variables are not simple objects)
 *
 * @returns {{load, check, state}} - Current request state
 * @deprecated This function is not needed since useGraphqlWatchQuery performs deep equality check
 * on variables
 */
export function useGraphqlWatchQueryVars<Vars, Data>({
  variableCreator,
  ...queryProps
}: IUseGraphWatchQueryVarsParams<Vars, Data>) {
  const variables = useMemo(...variableCreator);
  return useGraphqlWatchQuery({ ...queryProps, variables });
}
