import { DependencyList, useContext, useMemo } from 'react';
import {
  GraphqlQueryStore,
  IGraphqlQueryOptions,
  IGraphqlQueryStoreParams
} from '@proscom/prostore-apollo';
import {
  ProstoreSsrContext,
  UseRequestOptions,
  useRequestStore
} from '@proscom/prostore-react';
import { useContextApolloClient } from './ApolloClientsContext';
import { ApolloClient } from 'apollo-client';
import { PartialKey } from '@proscom/prostore';

export type IUseGraphqlQueryOptions<Vars, Data> = PartialKey<
  IGraphqlQueryStoreParams<Vars, Data>,
  'client'
>;

export interface IUseGraphqlQueryParams<Vars, Data> {
  /** Apollo Client instance to be used for data fetching, or its name for it to be injected from the context */
  client?: string | ApolloClient<any>;
  /** Query specification */
  queryOptions: IUseGraphqlQueryOptions<Vars, Data>;
  /** Query variables */
  variables?: Vars;
  /** Additional query options */
  options?: IGraphqlQueryOptions<Vars, Data>;
  /** Additional hook options */
  hookOptions?: UseRequestOptions<Vars, Data>;
}

/**
 * Creates GraphqlQueryStore and subscribes to it using useRequestStore.
 * Effectively this requests data from the apollo based on the provided variables and queryOptions
 *
 * @returns {{load, check, state}} - Current request state
 */
export function useGraphqlQuery<Vars, Data>({
  client,
  queryOptions,
  variables,
  options,
  hookOptions
}: IUseGraphqlQueryParams<Vars, Data>) {
  const actualClient = useContextApolloClient(client);
  const ssrContext = useContext(ProstoreSsrContext);

  const store = useMemo(
    () =>
      new GraphqlQueryStore({
        ssrContext,
        client: actualClient,
        ...queryOptions
      }),
    [ssrContext, actualClient, queryOptions]
  );

  return useRequestStore(store, variables, options, hookOptions);
}

export interface IUseGraphQueryVarsParams<Vars, Data>
  extends IUseGraphqlQueryParams<Vars, Data> {
  variableCreator: [() => Vars, DependencyList];
}

/**
 * The same as useGraphqlQuery, but it uses variableCreator which is supplied to useMemo and
 * can be used to memoize query variables. This may be used when the default behavior of deeply
 * comparing variables is not preferable (i.e., it is not fast enough, or variables are not simple objects)
 *
 * @returns {{load, check, state}} - Current request state
 * @deprecated This function is not needed since useGraphqlQuery performs deep equality check
 * on variables
 */
export function useGraphqlQueryVars<Vars, Data>({
  variableCreator,
  ...queryProps
}: IUseGraphQueryVarsParams<Vars, Data>) {
  const variables = useMemo(...variableCreator);
  return useGraphqlQuery({ ...queryProps, variables });
}
