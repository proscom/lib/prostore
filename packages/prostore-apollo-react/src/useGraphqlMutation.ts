import { useAsyncOperation, UseAsyncOperationOptions } from '@proscom/prostore-react';
import ApolloClient, {
  MutationOptions,
  OperationVariables
} from 'apollo-client';
import { useContextApolloClient } from './ApolloClientsContext';

/**
 * Allows to observe status of the GraphQL-mutation
 * @param clientOrName - ApolloClient instance to be used for data fetching, or its name for it to be injected from the context
 * @param options - Options are passed to ApolloClient's `mutate` method
 * @param hookOptions - Options are passed to useAsyncOperation hook
 * @returns [run, loading], where
 *  - `run` is the callback which accepts partial options which are passed to ApolloClient's `mutate` method
 *  - `loading` is the current state whether the async operation is running
 * @example
 *  const [run, loading] = useGraphqlMutation(null, { mutation: MUTATION });
 *  return <div>{loading ? 'Loading...' : <button onClick={() => run({ variables: { time: new Date() } })} />}
 */
export function useGraphqlMutation<Result = any, Variables = OperationVariables>(
  clientOrName: string | ApolloClient<any> | null | undefined = null,
  options: Partial<MutationOptions<Result, Variables>> = {},
  hookOptions: UseAsyncOperationOptions = {}
) {
  const client = useContextApolloClient(clientOrName);
  return useAsyncOperation(
    (mutationOptions: Partial<MutationOptions<Result, Variables>>) => {
      return client.mutate({
        ...options,
        ...mutationOptions
      } as MutationOptions<Result, Variables>);
    },
    hookOptions
  );
}
