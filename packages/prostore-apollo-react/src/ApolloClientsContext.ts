import { createContext, useContext } from 'react';
import ApolloClient from 'apollo-client';

export type IApolloClients = {
  [name: string]: ApolloClient<any>;
};

export type IExtractedData = {
  [name: string]: any;
};

/**
 * React context to hold ApolloClientsManager instance
 */
export const ApolloClientsContext = createContext<ApolloClientsManager|null>(null);

/**
 * Used as the IoC container to inject Apollo client instances into components
 * based on their names
 */
export class ApolloClientsManager {
  constructor(public clients: IApolloClients) {}

  getClient(key: string = 'default'): ApolloClient<any>|undefined {
    return this.clients[key];
  }

  extract(): IExtractedData {
    const result: IExtractedData = {};
    for (let [key, value] of Object.entries(this.clients)) {
      result[key] = value.extract();
    }
    return result;
  }
}

/**
 * Injects Apollo client from the context based on the provided name or
 * uses the provided instance. If called without arguments, uses the default
 * instance
 *
 * @param clientOrName - name of the Apollo client to be injected, or instance to be used
 */
export function useContextApolloClient(
  clientOrName?: string | ApolloClient<any> | null | undefined
): ApolloClient<any> {
  const apolloClientsContext = useContext(ApolloClientsContext);

  if (!clientOrName || typeof clientOrName === 'string') {
    if (!apolloClientsContext) {
      throw new Error(
        `ApolloClientsContext is not provided`
      );
    }
    const client = !clientOrName ? apolloClientsContext.getClient() : apolloClientsContext.getClient(clientOrName);
    if (!client) {
      throw new Error(
        `ApolloClient '${clientOrName}' is not registered in the ApolloClientsContext`
      );
    }
    return client;
  }

  return clientOrName;
}
