# `prostore-axios`

Эта библиотека расширяет [`prostore`](https://gitlab.com/proscom/prostore/-/blob/master/packages/prostore/README.md),
добавляя в него специальные классы сторов для выполнения http-запросов
с помощью [axios](https://github.com/axios/axios).

В библиотеку входит единственный класс `AxiosQueryStore`, который
расширяет `RequestStore`, обеспечивая единообразное API
для выполнения запросов и хранения их результатов в сторе,
на который можно подписаться.

## Использование

```typescript
import axios from 'axios';
import { AxiosQueryStore } from '@proscom/prostore-axios';

// Создаем клиент, который будет выполнять запросы
// на клиент можно навесить интерцепторов, чтобы расширить
// его функционал. Подробнее - в документации axios
const client = axios.create();

const query: AxiosRequestConfig = {
  url: '/countries',
  method: 'get'
};

// Создаем стор
const store = new AxiosQueryStore({
  // Запрос, который будет выполняться
  query,

  // Клиент, который будет выполнять запрос
  client,

  // Функция, которая по результату запроса result определяет,
  // что положить в state.data
  // (опционально, по умолчанию кладется весь результат)
  mapData: (x) => x.data,

  // Функция, которая превращает variables, переданные в стор,
  // в AxiosRequestConfig. Можно задать свою функцию, или использовать
  // AxiosQueryStore.mapVariablesParams или AxiosQueryStore.mapVariablesData
  mapVariables: (data) => {
    data;
  },

  // Так как AxiosQueryStore расширяет RequestStore
  // то можно передать все параметры конструктора RequestStore
  // см. подробнее в Readme @proscom/prostore
  // https://gitlab.com/proscom/prostore/-/blob/master/packages/prostore/README.md#requeststore

  // Первоначальное значение
  initialData: null,

  // Функция пропуска запроса в зависимости от переменных
  skipQuery: (vars) => undefined,

  // Функция обновления данных при повторном запросе
  updateData: (data, oldData, params) => data,

  // Уникальный идентификатор стора для передачи данных
  // при использовании серверного рендеринга
  ssrId: undefined
});

// Чтобы инициировать запрос данных, надо вызвать
store.loadData(
  // variables - изменяемые параметры запроса
  // можно передать любую часть параметров axios.request
  // они применятся поверх того, что определено в query
  {
    // например, get-параметры
    params: {
      page: 1
    }
  },

  // options - объект опций
  {
    // можно перезаписать состояние стора вручную
    changeState: {},
    // можно передать любую часть параметров axios.request
    // они применятся поверх того, что определено в query
    axiosOptions: {}
  }
);

// Можно подождать ответа сразу
try {
  const result = await store.loadData(/* ... */);
} catch (error) {
  // ...
}

// А можно подписаться на все изменения стора
store.state$.subscribe((state) => {
  const { loading, loaded, error, data, variables } = state;
  // ...
});
```

Для реализации бесконечной подгрузки, см. раздел
["Пагинация с дозагрузкой"](https://gitlab.com/proscom/prostore/-/blob/master/packages/prostore/README.md#%D0%BF%D0%B0%D0%B3%D0%B8%D0%BD%D0%B0%D1%86%D0%B8%D1%8F-%D1%81-%D0%B4%D0%BE%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%BE%D0%B9)
документации `@proscom/prostore`.
