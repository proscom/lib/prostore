import {
  IRequestStoreOptions,
  IRequestStoreParams,
  RequestStore
} from '@proscom/prostore';
import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

/**
 * Function which converts axios response to the store state data.
 * @param data - response.data
 * @param response - response
 */
export type IMapAxiosResponse<Data> = (
  data: any,
  response: AxiosResponse
) => Data;

export interface IAxiosQueryOptions<Vars, Data>
  extends IRequestStoreOptions<Vars, Data> {
  /** Additional Axios query options */
  axiosOptions?: AxiosRequestConfig;
}

export type IMapAxiosQueryVariables<Vars> = (
  variables: Vars | undefined,
  query: AxiosRequestConfig
) => AxiosRequestConfig;

export interface IAxiosQueryStoreParams<Vars, Data>
  extends IRequestStoreParams<Vars, Data> {
  /** Axios query definition */
  query?: AxiosRequestConfig;

  /** Axios client to be used for fetching the data */
  client: AxiosInstance;

  /**
   * Optional callback to transform the query result into the request store state.
   * Use it to remove unnecessary nesting or transform the data
   */
  mapData?: IMapAxiosResponse<Data>;

  /**
   * Optional callback to transform input variables into AxiosRequestConfig
   */
  mapVariables?: IMapAxiosQueryVariables<Vars>;
}

/**
 * RequestStore tailored to make Axios HTTP requests
 */
export class AxiosQueryStore<
  Vars = AxiosRequestConfig,
  Data = any
> extends RequestStore<Vars, Data, IAxiosQueryOptions<Vars, Data>> {
  client: AxiosInstance;
  mapData: IMapAxiosResponse<Data>;
  query: AxiosRequestConfig;
  mapVariables: IMapAxiosQueryVariables<Vars>;

  constructor({
    query,
    client,
    mapData = (x) => x,
    mapVariables = (x) => x || {},
    ...requestParams
  }: IAxiosQueryStoreParams<Vars, Data>) {
    super(requestParams);
    this.query = query || {};
    this.client = client;
    this.mapData = mapData;
    this.mapVariables = mapVariables;

    console.assert(
      client,
      'Parameter "client" is required for AxiosQueryStore'
    );
  }

  /**
   * Performs actual Axios HTTP request using the Axios Client
   *
   * @param variables - request variables
   * @param options - additional query options
   */
  async performRequest(
    variables: Vars,
    options: Partial<IAxiosQueryOptions<Vars, Data>>
  ): Promise<Data> {
    const baseQuery = {
      ...this.query,
      ...options.axiosOptions
    };

    const request: AxiosRequestConfig = {
      ...baseQuery,
      ...this.mapVariables(variables, baseQuery)
    };

    const result = await this.client.request(request);
    return this.mapData(result.data, result);
  }

  static mapVariablesParams: IMapAxiosQueryVariables<
    AxiosRequestConfig['params']
  > = (params) => ({ params });
  static mapVariablesData: IMapAxiosQueryVariables<
    AxiosRequestConfig['data']
  > = (data) => ({ data });
}
