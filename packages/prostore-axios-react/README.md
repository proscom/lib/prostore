# `prostore-axios-react`

Адаптер для
[prostore](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore)
, связывающий
[prostore-axios](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-axios)
и
[prostore-react](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-react).

Позволяет реактивно подключать результат HTTP запроса в React-компоненты.

Содержит две полезных фичи:

- `AxiosClientsContext` - прокидывание клиента Apollo через контекст
- `useAxiosQuery` - хук, который
  динамически создаёт AxiosQueryStore в соответствии с переданными
  параметрами и подписывает на него компонент.

## AxiosClientsContext

Позволяет прокидывать клиент axios через контекст реакта.
Таким образом реализуется принцип Dependency Injection, и
снижается связанность кода.

```tsx
// index.tsx
import axios from 'axios';
import {
  AxiosClientsContext,
  AxiosClientsManager
} from '@proscom/prostore-apollo-react';
import { App } from './App';

const defaultClient = axios.create();
const specificClient = axios.create({
  /* ... */
});

const axiosContext = new AxiosClientsManager({
  default: axiosClient,
  specific: axiosClient2
});

const rootElement = document.getElementById('root');

ReactDOM.render(
  <AxiosClientsContext.Provider value={axiosContext}>
    <App />
  </AxiosClientsContext.Provider>,
  rootElement
);
```

```tsx
// App.ts
function App() {
  const defaultClient = useContextAxiosClient();
  const specificClient = useContextAxiosClient('specific');

  // defaultClient === axiosClient
  // specificClient === axiosClient2

  // ...
}
```

## `useAxiosQuery`

Этот хук динамически создает стор
[AxiosQueryStore](https://gitlab.com/proscom/prostore/-/tree/master/packages/prostore-axios)
и подписывают компонент на этот стор с помощью
[`useRequestStore`](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-react#useRequestStore)
.
Параметры, переданные в queryOptions, передаются в конструктор `AxiosQueryStore`.

Данный хук позволяет компоненту при маунте выполнить HTTP-запрос с помощью axios,
и перерендерить компонент при получении результата. Запрос будет автоматически перевыполнен,
если компонент перерендерится с измененными переменными запроса.

```tsx
// Статические параметры запроса
const query: AxiosRequestConfig = {
  url: '/api/user',
  method: 'get'
};

// Динамические параметры запроса
interface UserQueryVars {
  id: number;
}

// Результат запроса
interface UserQueryData {
  name: string;
}

// Это по-сути аргумент конструктора AxiosQueryStore
// должен быть за пределами компонента (или в useMemo),
// чтобы сохранять ссылочную постоянность, т.к. стор
// будет пересоздаваться при каждом изменении queryOptions
const queryOptions: IAxiosQueryStoreParams<UserQueryVars, UserQueryData> = {
  // Параметры запроса, которые передаются в axios и не зависят от variables
  query,
  // Преобразовывает результат запроса в UserQueryData
  mapData: (result) => result.data,
  // Преобразовывает UserQueryVars в AxiosRequestConfig.
  // Если не задана, то преобразования не происходит и в качестве variables
  // необходимо передавать AxiosRequestConfig
  mapVariables: (params) => ({ params }),
  // Позволяет не выполнять запрос в зависимости от значений variables.
  // Здесь, если variables === null, то запрос не выполняется и data === null
  skipQuery: skipIfNull(null)
};

function UserInfo({ id }) {
  const userQuery = useAxiosQuery({
    queryOptions,
    // Сравниваются глубоко с помощью lodash.isEqual
    // если меняются, то запрос сам перевыполняется
    variables: { id },
    // Можно передать конкретный клиент Apollo, либо его ключ,
    // тогда он будет подключен из контекста
    client,
    // Дополнительные опции передаются в AxiosQueryStore.loadData
    options
  });
  const userData = userQuery.state.data;

  // С userQuery можно работать точно так же, как
  // с результатом вызова хука useRequestStore

  if (userQuery.check.spinner) {
    return <Spinner />;
  } else if (userQuery.check.error) {
    return <ErrorMessage error={userQuery.state.error} />;
  } else if (!userData) {
    return <ErrorMessage error={'Пользователь не найден'} />;
  }

  return <div>{userData.name}</div>;
}
```

Так как `useAxiosQuery` создаёт
стор внутри компонента, то при ремаунте компонента стор будет
создан заново. Это приводит к повторным запросам на бекенд,
поэтому рекомендуется использовать какой-нибудь кеш, например
[`axios-cache-adapter`](https://www.npmjs.com/package/axios-cache-adapter).

## Мутации

Хук `useAxiosQuery` представляет собой реактивную зависимость результата запроса от переменных (параметров запроса).
Это значит, что момент выполнения запроса определяется автоматически.
Такая семантика подходит только запросов, удовлетворяющим свойствам чистой функции,
например для GET HTTP запросов, возвращающих одни и те же значения для одних и тех же переменных
и не выполняющим побочных эффектов

Мутирующие запросы (POST HTTP) следует вызывать в коде императивно
в нужный момент (например, в ответ на клик пользователя по кнопке) с помощью функций
`axios`.
Мутирующий запрос может не оказывать никакого влияния на состояние компонентов, тогда
его можно вызывать просто напрямую без использования хуков из этой библиотеки.

Если же возникает потребность отслеживать статус выполнения мутирующего запроса, то
для этого можно использовать хук `useAsyncOperation` из
[@proscom/prostore-react](https://gitlab.com/proscom/prostore/-/tree/master/packages/prostore-react):

```tsx
import { useAsyncOperation } from '@proscom/prostore-react';
import { useContextAxiosClient } from './AxiosClientsContext';

function MyComponent() {
  const client = useContextAxiosClient();
  const saveOp = useAsyncOperation(
    () => {
      // Чтобы корректно отследить статус завершения операции,
      // колбек должен возвращать промис
      return client.post('/save', data).catch((e) => console.error(e));

      // То, как обрабатывать ошибку следует решить на уровне конкретного проекта.
      // Рекомендуется отобразить пользователю toast или текст ошибки рядом с кнопкой действия.
    },
    {
      // После завершения операции ставит finished=true на 5 секунд
      // Передайте Infinity, чтобы поставить finished=true навсегда после первого выполнения операции
      finishedTimeout: 5000,
      // Предотвращает повторный вызов операции до завершения предыдущей
      singleton: true
    }
  );

  const {
    // Функция для вызова операции
    run,
    // true, если операция выполняется
    loading,
    // true, если операция недавно завершена
    finished,
    // позволяет поменять значение finished в сложных случаях
    setFinished
  } = saveOp;

  return (
    <button onClick={run} disabled={loading}>
      {finished ? 'Saved' : loading ? 'Saving...' : 'Save'}
    </button>
  );
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-async-operation-tzlhm)

## Примеры использования

### Создание axios-клиента с автоповтором запросов при ошибке сети и кешем

```typescript
import axios from 'axios';
import axiosRetry from 'axios-retry';
import { setupCache } from 'axios-cache-adapter';

const clientCache = setupCache({
  // Время жизни в кеше - 10 минут
  maxAge: 10 * 60 * 1000,
  // Задаем лимит количества сохраненных записей в кеше, чтобы вся память не съедалась
  limit: 100 as any, // Тут проблема с типами в библиотеке
  exclude: {
    // Кешируем запросы вместе с квери-параметрами
    query: false
  }
});

export const defaultClient = axios.create({
  // Используем кеширующий адаптер
  adapter: clientCache.adapter
});

// Повторяем запросы при ошибке сети до тех пор, пока она не исчезнет
axiosRetry(defaultClient, {
  // Бесконечное количество повторов
  retries: Infinity,
  // Повторный запрос через 5 секунд
  retryDelay: () => 5000
});
```

При использовании кеша может потребоваться удалить какое-нибудь значение из кеша.
Для этого можно использовать следующий код:

```typescript
import { serializeQuery } from 'axios-cache-adapter';

function clearQueryFromCache(url) {
  clientCache.store.iterate((value, key) => {
    if (key.startsWith(url)) {
      clientCache.store.removeItem(key);
    }
  });
}

clearQueryFromCache('/api/user');
// todo добавить поддержку query-параметров в clearQueryFromCache
```

Также можно вызвать запрос с инвалидацией кеша, передав `ignoreCache: true` в `options`:

```tsx
function UserInfo({ id }) {
  const userQuery = useAxiosQuery({
    /* ... */
  });

  const reloadData = () => {
    userQuery.load({ ignoreCache: true });
  };
}
```

Можно совместить инвалидацию кеша с [шиной событий](https://gitlab.com/proscom/prostore/-/tree/master/packages/prostore#шина-событий).
Для разрешения наибольшего количества ситуаций рекомендуется и сбрасывать кеш вручную, и вызывать запрос заново:

```tsx
const EVENT_USER_RELOAD = 'EVENT_USER_RELOAD';

storeEvents$.subscribe((event) => {
  if (event.type === EVENT_USER_RELOAD) {
    clearQueryFromCache('/api/user');
  }
});

function UserInfo({ id }) {
  const userQuery = useAxiosQuery({ params: { id } /* ... */ });

  const reloadData = () => {
    userQuery.load({ ignoreCache: true });
  };

  useObservable(storeEvents$, (event) => {
    if (event.type === EVENT_USER_RELOAD && id === event.id) {
      reloadData();
    }
  });

  return <div>...</div>;
}

function UserReload({ id }) {
  const handleClick = () => {
    storeEvents$.next({ type: EVENT_USER_RELOAD, id });
  };
  return <button onClick={handleClick}>Reload</button>;
}
```

### Пагинация с дозагрузкой

В случаях когда нужна пагинация с дозагрузкой (например, бесконечный скролл с дозагрузкой)
можно использовать функцию `updateData`.

Пример:

```tsx
import {
  AxiosQueryStore,
  IAxiosQueryStoreParams
} from '@proscom/prostore-axios';

interface NewsQueryVars {
  page?: number;
  perPage?: number;
}

interface NewsItem {
  name: string;
}

type NewsQueryData = NewsItem[];

const queryOptions: IAxiosQueryStoreParams<NewsQueryVars, NewsQueryData> = {
  query: {
    url: '/news',
    method: 'get'
  },
  // Берем старые данные и соединяем с новыми в один большой массив
  updateData: (data, oldData, params) => {
    const page = params.variables.page;
    const perPage = params.variables.perPage;
    return [
      ...oldData?.slice(0, page * perPage),
      ...data,
      ...oldData?.slice((page + 1) * perPage)
    ];
  },
  mapVariables: AxiosQueryStore.mapVariablesParams
};

function MyComponent() {
  const [page, setPage] = useState(0);

  const query = useAxiosQuery({
    queryOptions,
    variables: {
      page,
      perPage: 10
    }
  });

  const handleLoadMore = () => {
    setPage((p) => p + 1);
  };

  // Не обязательно использовать query.check для определения того,
  // что рендерить. Можно создать свою комбинацию условий на основе
  // query.state

  return (
    <div>
      {query.state.data?.map((item, iItem) => {
        return <div key={iItem}>{item.name}</div>;
      })}
      {query.state.error && 'Error: ' + query.state.error}
      {query.state.loading ? (
        'Loading...'
      ) : (
        <button onClick={handleLoadMore}>Load More</button>
      )}
    </div>
  );
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-load-more-elds8?file=/src/App.tsx)
