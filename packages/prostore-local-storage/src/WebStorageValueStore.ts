import { StorageValueStore, StorageValueParser } from "./StorageValueStore";
import { WebStorageAdapter } from "./WebStorageAdapter";

/**
 * Как StorageValueStore, только со встроенным WebStorageAdapter.
 * См. документацию к StorageValueStore
 *
 * @example
 * const LOCAL_STORAGE_KEY_SCORE = 'score';
 *
 * // Синхронизированный стор
 * const scoreStore = new WebStorageValueStore<number>(
 *   localStorage,
 *   LOCAL_STORAGE_KEY_SCORE,
 *   // Трансформер значения. Так как внешнее хранилище может изменяться извне,
 *   // то нет гарантии что значение имеет определенный тип.
 *   // С помощью трансформера можно привести значение к нужному типу
 *   (value) => +value || 0
 * );
 *
 * // Регистрируем обработчик события storage
 * scoreStore.registerListener();
 *
 * // При изменении значения во внешнем хранилище, оно изменится и в сторе.
 * // Подписываемся на изменение значения как на любой обзервабл
 * const sub = scoreStore.state$.subscribe((score) => {
 *   // После первоначальной загрузки данных из внешнего хранилища loaded=true
 *   // score.value содержит непосредственно значение
 *   console.log(score.loaded, score.value);
 * });
 *
 * // Значение стора можно изменить, тогда оно меняется и во внешнем хранилище
 * scoreStore.setValue(5);
 *
 * // Отписаться от обновлений стора можно так
 * sub.unsubscribe();
 *
 * // Перед удалением scoreStore обработчик событий надо снять
 * scoreStore.destroy();
 */
export class WebStorageValueStore<T> extends StorageValueStore<T> {

  /**
   * @param storage - объект localStorage или sessionStorage
   * @param key - ключ по которому хранится значение
   * @param parseValue - трансформер для значения.
   *  Так как внешнее хранилище может изменяться извне,
   *  то нет гарантии что значение имеет определенный тип.
   *  С помощью трансформера можно привести значени к нужному типу
   */
  constructor(
    storage: Storage,
    key: string,
    parseValue: StorageValueParser<T>
  ) {
    super(
      new WebStorageAdapter(storage, key),
      parseValue
    );
  }
}
