import { defer, fromEvent, merge, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { IStorageValueAdapter } from './types';
import { tryParseJson } from '@proscom/ui-utils';

/**
 * Адаптер для взаимодействия со значением в WebStorage (localStorage).
 *
 * Предназначен для использования вместе с StorageValueStore.
 *
 * В большинстве случаев вместо прямого использования этого класса
 * достаточно использовать WebStorageValueStore.
 *
 * @example
 * const adapter = new WebStorageAdapter(localStorage, 'test');
 * const sub = adapter.value$.subscribe((value) => console.log(value));
 * adapter.setValue('newValue');
 * sub.unsubscribe();
 *
 */
export class WebStorageAdapter<T = any> implements IStorageValueAdapter<T> {
  /**
   * Обзервабл на значение ключа из localStorage.
   * Не эмитит значения, если ключ изменён с помощью setValue
   */
  public value$ = merge(
    defer(() => of(this.storage.getItem(this.key))),
    fromEvent<StorageEvent>(window, 'storage').pipe(
      filter((e: StorageEvent) => {
        return e.key === this.key && e.newValue !== e.oldValue;
      }),
      map((e: StorageEvent) => e.newValue)
    )
  ).pipe(map((value) => tryParseJson<T>(value)));

  /**
   * @param storage - объект localStorage или sessionStorage
   * @param key - ключ по которому хранится значение
   */
  constructor(public storage: Storage, public key: string) {}

  setValue(value: T | null) {
    if (value !== null) {
      this.storage.setItem(this.key, JSON.stringify(value));
    } else {
      this.storage.removeItem(this.key);
    }
  }
}
