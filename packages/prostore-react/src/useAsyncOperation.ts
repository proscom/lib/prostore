import { useCallback, useState } from 'react';
import {
  useLatestCallbackRef,
  useMountedRef,
  usePropsRef,
  useTimeoutRef
} from '@proscom/ui-react';

export interface UseAsyncOperationOptions {
  finishedTimeout?: number;
  singleton?: boolean;
}

export interface UseAsyncOperationState {
  loading: number;
  finished: boolean;
}

const changeLoading = (change: number) => (state: UseAsyncOperationState) => ({
  ...state,
  loading: state.loading + change
});

const changeFinished = (finished: boolean) => (
  state: UseAsyncOperationState
) => ({
  ...state,
  finished
});

/**
 * This error is thrown when attempt is made to call the singleton async operation
 * while previous call to it is still running.
 * Usually you should catch and swallow this error.
 *
 * @example
 *  run().then(() => {}).catch(e => {
 *    if (e instanceof AsyncSingletonError) return;
 *    toast.error(e);
 *  });
 */
export class AsyncSingletonError extends Error {
  constructor(m?: string) {
    super(m);
    Object.setPrototypeOf(this, AsyncSingletonError.prototype);
  }
}

/**
 * Allows to observe the status of async operation (loading, finished)
 * @param callback - async callback which performs the operation
 * @param options - hook options:
 *  - singleton - prevents calling operation until previous call is finished
 *      on the second call, rejects the promise with the instance of AsyncSingletonError
 *  - finishedTimeout - if set, after operation is completed, sets value of
 *      finished to true for the given number of milliseconds. To set finished
 *      permanently, set finishedTimeout to Infinity
 * @example
 *  const [run, loading] = useAsyncOperation(() => fetch('url').catch(e => console.error(e)));
 *  return <div>{loading ? 'Loading...' : <button onClick={run} />}
 */
export function useAsyncOperation<Args extends any[], Result>(
  callback: (...args: Args) => Promise<Result>,
  options: UseAsyncOperationOptions = {}
) {
  const [state, setState] = useState<UseAsyncOperationState>({
    loading: 0,
    finished: false
  });
  const timeoutRef = useTimeoutRef();
  const mountedRef = useMountedRef();
  const callbackRef = useLatestCallbackRef(callback);
  const latestRef = usePropsRef({
    loading: state.loading,
    options
  });

  const run = useCallback(
    (...args: Args): Promise<Result> => {
      const { options, loading } = latestRef;
      if (options.singleton && loading > 0) {
        return Promise.reject(
          new AsyncSingletonError(
            'useAsyncOperation.run called for the singleton operation ' +
              'while previous instance is still running. ' +
              'Usually you should catch and swallow this error.'
          )
        );
      }

      setState(changeLoading(1));

      return callbackRef(...args).then(
        (result) => {
          if (mountedRef.current) {
            setState(changeLoading(-1));
            if (
              options.finishedTimeout !== undefined &&
              options.finishedTimeout > 0
            ) {
              setState(changeFinished(true));
              if (options.finishedTimeout !== Infinity) {
                timeoutRef.set(() => {
                  setState(changeFinished(false));
                }, options.finishedTimeout);
              }
            }
          }
          return result;
        },
        (e) => {
          if (mountedRef.current) {
            setState(changeLoading(-1));
          }
          throw e;
        }
      );
    },
    [latestRef, timeoutRef]
  );

  const setFinished = useCallback((finished: boolean) => {
    setState(changeFinished(finished));
  }, []);

  return {
    run,
    loading: state.loading > 0,
    finished: state.finished,
    setFinished
  };
}
