import { useMemo, useState } from 'react';
import { StoreOrName, useContextStore } from './ProstoreContext';
import { useConnectStore } from './useConnectStore';
import { IStore, IStoreState } from '@proscom/prostore';

/**
 * Subscribe component to the store
 *
 * @param storeOrName - Store instance or name for it to be injected from the context
 * @returns
 *  state - local copy of store state;
 *  store - instance of the store
 */
export function useStore<Store extends IStore<any>>(
  storeOrName: StoreOrName<Store>
): [IStoreState<Store>, Store] {
  const store = useContextStore<Store>(storeOrName);
  const [state, setState] = useState(store.state);
  useConnectStore(store, setState);
  return [state, store];
}

/**
 * Subscribe component to the store
 *
 * @param storeOrName - Store instance or name for it to be injected from the context
 * @returns local copy of store state
 */
export function useStoreState<State>(storeOrName: StoreOrName<IStore<State>>): State {
  const store = useContextStore(storeOrName);
  const [state, setState] = useState(store.state);
  useConnectStore(store, setState);
  return state;
}
