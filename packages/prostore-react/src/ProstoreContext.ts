import { useContext, createContext } from 'react';
import { IStore } from '@proscom/prostore';

export type IProstoreContext = {
  [name: string]: IStore<any>;
};

export type StoreOrName<Store> = string | Store;

/**
 * React context which holds mapping of store names to their instances.
 * Used as the IoC container for store injection into the components
 */
export const ProstoreContext = createContext<IProstoreContext | null>(null);

/**
 * Extracts store from the context based on the provided name,
 * or simply uses the provided instance
 *
 * @param storeOrName - store instance to be used, or store name to be injected from the context
 */
export function useContextStore<Store extends IStore<any>>(
  storeOrName: StoreOrName<Store>
): Store {
  const context = useContext(ProstoreContext);

  if (typeof storeOrName === 'string') {
    if (!context) {
      throw new Error(`ProstoreContext is not provided`);
    }
    const store = context[storeOrName];
    if (!store) {
      throw new Error(
        `Store '${storeOrName}' is not registered in the ProstoreContext`
      );
    }
    return store as Store;
  }

  return storeOrName;
}
