import { IStore } from '@proscom/prostore';
import { useObservable } from './useObservable';

/**
 * Subscribes to the provided store
 */
export function useConnectStore<State = any>(
  store: IStore<State>,
  setState: (state: State) => any
) {
  useObservable(store.state$, setState);
}
