# `prostore-react`

Данный адаптер предоставляет набор полезных инструментов
для интеграции `prostore` в [`React`](https://reactjs.org/).

Можно прокидывать сторы через контекст и
подписываться на сторы с помощью хуков,
а также автоматически выполнять запросы в `RequestStore`.

## Использование

Для примеров ниже будет использован простой стор,
считающий количество вызовов функции `increment`:

```typescript
// IncStore.ts
import { BehaviorStore } from '@proscom/prostore';

export interface IncStoreState {
  number: number;
}

const initialState: IncStoreState = {
  number: 0
};

export class IncStore extends BehaviorStore<IncStoreState> {
  constructor() {
    super(initialState);
  }

  increment() {
    this.setState({
      number: this.state.number + 1
    });
  }
}
```

```typescript
// incStore.ts
import { IncStore } from './IncStore';
export const incStore = new IncStore();
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/stores/IncStore.ts)

### `useStore`

Основной хук, который позволяет подписать компонент на
все изменения стора

```tsx
import { useStore } from '@proscom/prostore-react';

// Импортируем любой store, расширяющий IStore
import { incStore } from './incStore';

function MyComponent() {
  const [state, store] = useStore(incStore);

  // state - состояние стора
  // store - инстанс стора, по которому можно вызывать методы

  // в этом примере store === incStore, но ниже есть пример, когда
  // useStore используется в связке с контекстом, где это не так

  const onClick = () => {
    store.increment();
  };

  return <button onClick={onClick}>{state.number}</button>;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/demo/Demo1.tsx)

### `useStoreState`

Иногда сам стор не нужен, а нужно только его состояние.
Тогда можно использовать этот хук, чтобы не делать лишнюю
деструктуризацию:

```tsx
function MyComponent() {
  const state = useStoreState(incStore);
  return <div>{state.number}</div>;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/demo/Demo2.tsx)

### `ProstoreContext`

Контекст позволяет прокидывать инстансы сторов в компоненты
по ключам. Таким образом реализуется принцип Dependency Injection
и снижается связанность компонентов и сторов.

```typescript
// stores.ts
export const STORE_TEST = 'STORE_TEST';
```

```tsx
// index.ts
import ReactDOM from 'react-dom';
import { STORE_TEST } from './stores';
import { IncStore } from './IncStore';
import { App } from './App';
import { ProstoreContext } from '@proscom/prostore-react';

const testStore = new IncStore();

// В коде компонентов можно будет подключить
// инстанс testStore по ключу STORE_TEST
const stores = {
  [STORE_TEST]: testStore
};

ReactDOM.render(
  <ProstoreContext.Provider value={stores}>
    <App />
  </ProstoreContext.Provider>,
  document.getElementById('root')
);
```

```tsx
// App.ts
import { useStore } from '@proscom/prostore-react';
import { IncStore } from './IncStore';
import { STORE_TEST } from './stores';

function App() {
  // Так как в контексте сохранено, что STORE_TEST это incStore,
  // то этот вызов полностью аналогичен useStore(incStore)

  // При использовании с TypeScript необходимо указать тип стора
  // в качестве аргумента дженерика. Проще всего указать имя класс стора (IncStore).
  // При необходимости можно уменьшить связанность кода, реализовав
  // отдельный интерфейс, который реализуется классом стора, и указать
  // его здесь в качестве аргумента дженерика.
  const [state, store] = useStore<IncStore>(STORE_TEST);

  const onClick = () => {
    store.increment();
  };

  return <button onClick={onClick}>{state.number}</button>;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/demo/Demo3.tsx)

### `useContextStore`

Иногда состояние стора в компоненте вообще не нужно,
а нужен только сам стор для вызова его методов.
При использовании сторов как глобальных переменных можно просто
вызывать их методы:

```tsx
import { incStore } from './incStore';

function IncButton() {
  return <button onClick={() => incStore.increment()}>Increment</button>;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/demo/Demo4.tsx)

При использовании контекста можно получить доступ к стору
с помощью хука `useContextStore`. По сравнению с использованием
`useStore` это избавит компонент от лишних перерендеров при изменении
состояния стора.

```tsx
import { useContextStore } from '@proscom/prostore-react';
import { IncStore } from './IncStore';
import { STORE_TEST } from './stores';
function IncButton() {
  // При использовании с TypeScript необходимо указать тип стора
  // в качестве аргумента дженерика. Проще всего указать имя класс стора (IncStore).
  // При необходимости можно уменьшить связанность кода, реализовав
  // отдельный интерфейс, который реализуется классом стора, и указать
  // его здесь в качестве аргумента дженерика.
  const incStore = useContextStore<IncStore>(STORE_TEST);
  return <button onClick={() => incStore.increment()}>Increment</button>;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-basic-gsx5u?file=/src/demo/Demo5.tsx)

### `useRequestStore`

При использовании RequestStore можно воспользоваться специальным
хуком `useRequestStore`, чтобы автоматически выполнять запрос
при изменении его переменных, а также отслеживать состояние запроса
(идет загрузка, ошибка, готов результат).

```tsx
import { AxiosQueryStore } from '@proscom/prostore-axios';
import { useRequestStore } from '@proscom/prostore-react';

// Для примера воспользуемся AxiosQueryStore из @proscom/prostore-axios
// Этот класс расширяет RequestStore реализуя выполнение
// http-запросов с помощью axios

// Определяем динамические параметры запроса
export interface DataQueryStoreVariables {
  params?: {
    page?: number;
  };
}

// Определяем тип результата запроса
export interface DataQueryStoreData {
  message: string;
}

// Создаём стор, который будет хранить результат запроса.
// Сам запрос вызовется, только при подключении стора в компонент
const store = new AxiosQueryStore<DataQueryStoreVariables, DataQueryStoreData>({
  client: axios.create(),
  query: {
    url: '/data.json',
    method: 'get'
  }
});

function MyComponent() {
  // Параметры запроса
  const variables = {
    params: {
      page: 1
    }
  };

  // Дополнительные опции
  const options = undefined;

  // Подключаем стор в компонент и выполняем запрос при необходимости
  // По факту выполнения запроса компонент ререндерится
  const query = useRequestStore(store, variables, options);

  const { check, state, load } = query;

  // check - утилитарный объект, который позволяет
  //  быстро определить, что надо рендерить

  // state - состояние RequestStore

  // load - функция, которая позволяет повторить запрос
  //  с теми же переменными

  if (check.spinner) {
    return <Spinner />;
  } else if (check.error) {
    return <ErrorMessage error={state.error} />;
  }

  return <Info data={state.data} refresh={load} />;
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-use-request-store-py67z?file=/src/demo/Demo1.tsx)

При каждом рендере `useRequestStore` **глубоко** сравнивает
предыдущие переменные с новыми, и если есть отличия, то
вызывает выполнение запроса с новыми переменными.
Под глубоким сравнением понимается
[`lodash.isEqual`](https://lodash.com/docs/4.17.15#isEqual).

`options` передаются вторым аргументом в `store.loadData`
в неизменном виде. При изменении `options` запрос не будет повторен.

`useRequestStore` можно также использовать с контекстом.
В таком случае необходимо указать тип переменных и тип результата аргументами дженерика:

```tsx
import { useRequestStore } from '@proscom/prostore-react';
import { STORE_TEST } from './stores';
import { StoreTestVariables, StoreTestData } from './stores/StoreTest';

function MyComponent() {
  const variables = {};
  const query = useRequestStore<StoreTestVariables, StoreTestData>(
    STORE_TEST,
    variables
  );
  // ...
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-use-request-store-py67z?file=/src/demo/Demo2.tsx)

### `useAsyncOperation`

`RequestStore` представляет собой реактивную зависимость результата запроса от переменных (параметров запроса).
Это значит, что момент выполнения запроса определяется автоматически.
Такая семантика подходит только запросов, удовлетворяющим свойствам чистой функции
(возвращающих одни и те же значения для одних и тех же переменных и не выполняющим побочных эффектов),
например для GET HTTP запросов на получение данных и запросов GraphQL Query.

Мутирующие запросы (POST HTTP или GraphQL Mutation) следует вызывать в коде императивно
в нужный момент (например, в ответ на клик пользователя по кнопке).
Мутирующий запрос может не оказывать никакого влияния на состояние компонентов, тогда
его можно вызывать просто напрямую без использования хуков из этой библиотеки.

Если же возникает потребность отслеживать статус выполнения мутирующего запроса, то
для этого можно использовать хук `useAsyncOperation`:

```tsx
import axios from 'axios';
import { useAsyncOperation, AsyncSingletonError } from '@proscom/prostore-react';

function MyComponent() {
  const saveOp = useAsyncOperation(
    () => {
      // Чтобы корректно отследить статус завершения операции,
      // колбек должен возвращать промис
      return axios.post('/save', data).catch((e) => console.error(e));

      // То, как обрабатывать ошибку следует решить на уровне конкретного проекта.
      // Рекомендуется отобразить пользователю toast или текст ошибки рядом с кнопкой действия.
    },
    {
      // После завершения операции ставит finished=true на 5 секунд
      // Передайте Infinity, чтобы поставить finished=true навсегда после первого выполнения операции
      finishedTimeout: 5000,
      // Предотвращает повторный вызов операции до завершения предыдущей.
      // При использовании этого параметра в случае повторного вызова будет выброшена ошибка AsyncSingletonError.
      // Её надо обработать
      singleton: true
    }
  );

  const {
    // Функция для вызова операции
    run,
    // true, если операция выполняется
    loading,
    // true, если операция недавно завершена
    finished,
    // позволяет поменять значение finished в сложных случаях
    setFinished
  } = saveOp;

  const handleClick = () => {
    // Для обработки ошибки AsyncSingletonError достаточно добавить catch при вызове `run`, 
    // который и так должен быть, чтобы избежать более серьезной проблемы с `Unhandled Rejection`
    run().catch((err) => {
      // Ошибку AsyncSingletonError достаточно просто поглотить, никак не обрабатывая.
      // Эта ошибка выбрасывается, если передан параметр `singleton: true`,
      // и функция `run` вызвана второй раз до завершения предыдущего вызова.
      if (err instanceof AsyncSingletonError) return;
      console.error(err);
    });
  };

  return (
    <button type="button" onClick={handleClick} disabled={loading}>
      {finished ? 'Saved' : loading ? 'Saving...' : 'Save'}
    </button>
  );
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-async-operation-tzlhm)

### `usePropsObservable`

Преобразовывает пропы компонента или любые другие данные, связанные с циклом рендера реакта,
в Observable из rxjs.
Этот Observable затем можно использовать для построения реактивных пайплайнов, и дальнейшей
подписки этого или другого компонента на результат.

Обратите внимание, что если подписать тот же самый компонент, на обзервабл, который возвращается
из этого хука, то это может привести к бесконечному циклу перерендеров, если передаваемые
пропы не стабилизируются. При передаче объект с пропами поверхностно проверяется
(см. [shallowequal](https://www.npmjs.com/package/shallowequal)).
Если ни один ключ не изменился, то обзервабл не будет эмитить новое значение.

Так как этот хук достаточно сложен для понимания и несет за собой накладные расходы при выполнении,
то использовать его следует только в том случае, если нужно связать компонент с каким-то существующим
обзерваблом, либо использовать специфичный функционал rxjs.

```tsx
import {
  useObservableState,
  usePropsObservable
} from '@proscom/prostore-react';
import React, { useState } from 'react';
import { debounceTime, map, tap } from 'rxjs/operators';

export default function App() {
  const [search, setSearch] = useState('');
  console.log('render', search);

  const debounced$ = usePropsObservable(
    // Значения, которые передаются в props$
    { search },
    // Конструктор обзервабла, возвращает debounced$
    (props$) => {
      return props$.pipe(
        debounceTime(500),
        map((p) => p.search),
        tap((v) => console.log('debounced', v))
      );
    },
    // Зависимости, от которых зависит вызов функции-конструктора.
    // Проверяются аналогично useMemo
    []
  );

  // Подписываемся на debounced$
  const debouncedSearch = useObservableState(debounced$);

  return (
    <div>
      <div>
        <input value={search} onChange={(e) => setSearch(e.target.value)} />
      </div>
      <div>{debouncedSearch}</div>
    </div>
  );
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-props-observable-d05ot?file=/src/App.tsx)

### `useObservable`

Этот хук позволяет подписать колбек на произвольный обзервабл.

```tsx
import { useObservable } from '@proscom/prostore-react';
import { fromEvent } from 'rxjs';

const resize$ = fromEvent(window, 'resize');

function MyComponent() {
  const handleChange = useCallback((value) => {
    console.log('changed', value);
  });
  useObservable(resize$, handleChange);

  // ...
}
```

### `useObservableCallback`

Этот хук позволяет подписаться на произвольный обзервабл, но не выполняет переподписку
при изменении колбека. Поэтому пример выше может быть переписан следующим образом:

```tsx
import { useObservableCallback } from '@proscom/prostore-react';
import { fromEvent } from 'rxjs';

const resize$ = fromEvent(window, 'resize');

function MyComponent() {
  useObservableCallback(resize$, (value) => {
    console.log('changed', value);
  });

  // ...
}
```

Этот хук полезен при создании подписки на события сторов, а не на их состояния.
См. пример в документации [`prostore`](https://gitlab.com/proscom/prostore/-/tree/master/packages/prostore).

### `useObservableState`

Этот хук позволяет подписать компонент на произвольный обзервабл.
При наступлении события (изменении обзервабла), его данные будут сохранены в стейт компонента,
а компонент перерендерится.

Убедитесь, что используемый обзервабл повторяет своё последнее значение при подписке,
в противном случае в редких ситуациях часть значений может быть потеряна.
Чтобы создать обзервабл, повторяющий свои значения, воспользуйтесь оператором
`shareReplay`. `BehaviorSubject` повторяет свои значения при подписке.

```tsx
import { useObservableState } from '@proscom/prostore-react';
import { fromEvent } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

const windowSize$ = fromEvent(window, 'resize').pipe(
  map(() => window.innerWidth),
  shareReplay()
);

function MyComponent() {
  const windowWidth = useObservableState(windowSize$, window.innerWidth);
  // ...
}
```

### `useSubject`

Этот хук аналогичен `useObservableState`, только принимает не просто `Observable`,
а кастомный тип `ObservableWithValue`, и автоматически задает первоначальное значение стейта компонента.

Убедитесь, что используемый обзервабл повторяет своё последнее значение при подписке,
в противном случае в редких ситуациях часть значений может быть потеряна.
Чтобы создать обзервабл, повторяющий свои значения, воспользуйтесь оператором
`shareReplay`. `BehaviorSubject` повторяет свои значения при подписке.

```tsx
import { useSubject } from '@proscom/prostore-react';
import { BehaviorSubject } from 'rxjs';

const data$ = new BehaviorSubject(5);

function MyComponent() {
  const data = useSubject(data$);
  // ...
}
```
