import { from, isObservable, PartialObserver, Subscribable, Unsubscribable } from "rxjs";
import { auditTime } from 'rxjs/operators';

export type Subscriber<T> = ((value: T) => void) | PartialObserver<T>;

export class SubscriptionManager {
  subscriptions: Unsubscribable[] = [];

  subscribe = <T>(obs$: Subscribable<T>, callback?: Subscriber<T>) => {
    const sub =
      // Get around calling ts function overload with union argument
      typeof callback === 'function'
        ? obs$.subscribe(callback)
        : obs$.subscribe(callback);
    this.subscriptions.push(sub);
    return sub;
  };

  subscribeAsync = <T>(obs$: Subscribable<T>, callback?: Subscriber<T>) => {
    const oobs$ = isObservable(obs$) ? obs$ : from(obs$);
    return this.subscribe(oobs$.pipe(auditTime(0)), callback);
  };

  unsubscribe(sub: Unsubscribable) {
    sub.unsubscribe();
    const index = this.subscriptions.indexOf(sub);
    if (index >= 0) {
      this.subscriptions.splice(index, 1);
    }
  }

  destroy() {
    for (const sub of this.subscriptions) {
      sub.unsubscribe();
    }
    this.subscriptions.length = 0;
  }
}
