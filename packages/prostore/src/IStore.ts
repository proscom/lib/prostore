import { Observable } from 'rxjs';

/**
 * Base interface for all Prostore stores
 */
export interface IStore<State> {
  /** Get current store state */
  readonly state: State;
  /** Get store state observable to subscribe to */
  readonly state$: Observable<State>;
}

/**
 * Tries to extract store state type based off the store type
 */
export type IStoreState<Store> = Store extends IStore<infer State>
  ? State
  : unknown;
