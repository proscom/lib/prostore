import { Observable } from "rxjs";

/**
 * Observable which can return current value.
 *
 * It is more general than BehaviorSubject because id does not guarantee
 * that value is related to the original observable computation scheme
 */
export interface ObservableWithValue<T> extends Observable<T> {
  readonly value: T;
}

/**
 * Extends an observable with the getter function for its current value
 *
 * @param obs$ - original observable
 * @param valueFn - getter function which should return the current value
 *
 * @example
 *  // original$ - some other ObservableWithValue
 *  const obs$ = attachValueToObservable(
 *    original$.pipe(map(transform)),
 *    () => transform(original$.value)
 *  );
 *
 *  // obs$.value == transform(original$.value)
 */
export function attachValueToObservable<T>(
  obs$: Observable<T>,
  valueFn: () => T
): ObservableWithValue<T> {
  Object.defineProperty(obs$, 'value', {
    get: valueFn
  });
  return obs$ as ObservableWithValue<T>;
}

