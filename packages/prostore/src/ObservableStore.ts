import { Subscribable, Unsubscribable } from 'rxjs';
import { ValueStore } from './ValueStore';

export class ObservableStore<Data> extends ValueStore<Data> {
  private unsub?: Unsubscribable;

  constructor(private readonly obs$: Subscribable<Data>, initialValue: Data) {
    super(initialValue);
  }

  onSubscribe() {
    this.unsub = this.obs$.subscribe(this.state$);
  }

  onUnsubscribe() {
    this.unsub?.unsubscribe();
  }
}
