export interface IProstoreSsrContext {
  /** Whether the application is running on the server or in the browser */
  readonly isServer: boolean;

  /**
   * Returns the saved state for the store with the provided ssrId
   * @param key - ssrId of the store
   * @returns saved state for the provided store
   */
  getState(key: string): any;

  /**
   * Saves the state of the store with the provided ssrId
   * @param key - ssrId of the store
   * @param value - state to be saved
   */
  setState(key: string, value: any);

  /**
   * Registers the promise from the store for it to be awaited during SSR
   *
   * @param promise - promise to be registered
   */
  registerPromise(promise: Promise<any>);
}
