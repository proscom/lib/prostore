import { useContext } from 'react';
import { __RouterContext as RouterContext } from 'react-router';

/**
 * Инжектирует контекст react-router в компонент
 */
export function useReactRouter() {
  return useContext(RouterContext);
}
