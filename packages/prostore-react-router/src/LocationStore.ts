import { attachValueToObservable, BehaviorStore } from '@proscom/prostore';
import { parse as parseQuery, stringify as stringifyQuery } from 'query-string';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import isEqual from 'lodash/isEqual';
import { History, Location } from 'history';
import {
  IQueryTransformer,
  IQueryTransformers,
  IQueryValue,
  ITransformedQuery,
  parseQueryWithTransformers,
  stringifyQueryWithTransformers
} from './transformers';
import { createUrlPreserver } from './createUrlPreserver';
import { createBatcher } from './createBatcher';

/**
 * Состояние LocationStore
 */
export interface ILocationStoreState {
  /**
   * Текущий Location, как в react-router
   */
  location: Location | null;

  /**
   * Десериализованные значения квери-параметров
   */
  query: ITransformedQuery | null;

  /**
   * Сериализованные значения квери-параметров
   */
  rawQuery: { [key: string]: IQueryValue } | null;
}

/**
 * Аргументы конструктора LocationStore
 */
export interface ILocationStoreArgs {
  /**
   * History из пакета history, для того, чтобы вызывать
   * history.push и history.replace
   */
  history: History;

  /**
   * Набор трансформеров квери-параметров
   */
  transformers?: IQueryTransformers;
}

/**
 * Часть десериализованных квери-параметров
 */
export type QueryPart<
  Params extends ITransformedQuery = ITransformedQuery,
  Keys extends readonly string[] = readonly string[]
> = {
  [key in Keys[number]]?: Params[key];
};

/**
 * Стор, обеспечивающий синхронизацию react-router и prostore
 */
export class LocationStore extends BehaviorStore<ILocationStoreState> {
  // History из пакета history, переданная в конструкторе стора
  history: History;

  // Набор активных трансформеров квери-параметров
  transformers: IQueryTransformers;

  constructor({ history, transformers }: ILocationStoreArgs) {
    super({
      location: null,
      rawQuery: null,
      query: null
    });

    this.history = history;
    this.transformers = transformers ? { ...transformers } : {};
  }

  /**
   * Позволяет добавить трансформер квери-параметра после создания стора
   * @param key - квери-параметр
   * @param transformer - трнасформер
   */
  setTransformer(key: string, transformer: IQueryTransformer | undefined) {
    if (!transformer) {
      delete this.transformers[key];
    } else {
      this.transformers[key] = transformer;
    }
  }

  /**
   * Функция для внутреннего использования. Принимает новый Location.
   * Должна вызываться при каждом изменении Location в react-router.
   * Вызывается из LocationProvider
   *
   * @param location - новый Location
   */
  updateLocation(location: Location) {
    const parsedQuery = parseQuery(location.search);
    const query = parseQueryWithTransformers(parsedQuery, this.transformers);

    this.setState({
      location,
      rawQuery: parsedQuery,
      query
    });
  }

  /**
   * Позволяет изменить квери-параметры в URL.
   * Принимает квери-параметры для изменения.
   * Все остальные квери-параметры будут сохранены.
   * Если необходимо убрать какой-то параметр, то в качестве значения следует передать undefined.
   *
   * По-умолчанию делает history.push (т.е. сохраняет навигацию в истории браузера для кнопки назад).
   *
   * @param changes - изменения квери-параметров
   * @param replace - если true, то вместо history.push происходит history.replace
   */
  changeQuery = createBatcher(
    (
      data: [query: ITransformedQuery, replace: boolean] | null,
      changes: Partial<ITransformedQuery>,
      replace = false
    ) => {
      const query = (data && data[0]) || this.state.query;

      const newQuery = {
        ...query,
        ...changes
      };

      return [newQuery, data ? data[1] && replace : replace];
    },
    ([query, replace]: [query: ITransformedQuery, replace: boolean]) => {
      const { location } = this.state;

      const convertedQuery = stringifyQueryWithTransformers(
        query,
        this.transformers
      );

      const newLocation = {
        ...location!,
        search: stringifyQuery(convertedQuery)
      };

      this.updateLocation(newLocation);
      if (replace) {
        this.history.replace(newLocation);
      } else {
        this.history.push(newLocation);
      }
    }
  );

  /**
   * Возвращает обзвервабл на часть квери-параметров
   * @param items - набор квери параметров, на которые надо подписаться
   * @returns обзервабл, содержащий трансформированные значения квери-параметров
   */
  get$<
    Params extends ITransformedQuery = ITransformedQuery,
    Keys extends readonly string[] = readonly string[]
  >(...items: Keys) {
    const transformQuery = (state: ILocationStoreState) => {
      const result: QueryPart<Params, Keys> = {};
      if (state.query) {
        for (let item of items) {
          if (item in state.query) {
            result[item] = state.query[item];
          } else {
            const transformer = this.transformers[item];
            result[item] = transformer ? transformer.parse() : undefined;
          }
        }
      }
      return result;
    };

    const result$ = this.state$.pipe(
      map(transformQuery),
      distinctUntilChanged<QueryPart<Params, Keys>>(isEqual)
    );

    return attachValueToObservable(result$, () => transformQuery(this.state));
  }

  /**
   * Создает конструктор ссылок, сохраняющих набор параметров адресной строки
   *
   * @param query - набор десериализованных квери-параметров для сохранения.
   *  По-умолчанию сохраняет все текущие параметры
   */
  createUrlPreserver(query: ITransformedQuery | null | undefined) {
    return createUrlPreserver(
      query || this.state.query || {},
      this.transformers
    );
  }
}
