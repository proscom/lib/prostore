import { ParsedQuery, StringifiableRecord } from "query-string";

/**
 * Значение, которое может иметь квери-параметр.
 * То что может вернуть функция parseQuery
 */
export type IQueryValue = string | string[] | null | undefined;

/**
 * Трансформер квери-параметра
 */
export interface IQueryTransformer<T = any> {
  /**
   * Десериализатор
   * @param queryValue - значение квери-параметра в url или undefined, если его нет
   * @returns значение после десериализации
   */
  parse: (queryValue?: IQueryValue) => T;

  /**
   * Сериализатор
   * @param value - значение, передаваемое в changeQuery
   * @returns значение, сериализованное в строку или undefined, если параметр не нужен в адресной строке
   */
  stringify: (value: T) => IQueryValue | undefined;
}

/**
 * Набор трансформеров для квери-параметров
 */
export interface IQueryTransformers {
  [key: string]: IQueryTransformer;
}

/**
 * Десериализованные значения квери-параметров
 */
export interface ITransformedQuery {
  [key: string]: any;
}

/**
 * Десериализует квери-параметры
 *
 * @param rawQuery - строковые квери-параметры из результата вызова parse из query-string
 * @param transformers - набор трансформеров
 */
export function parseQueryWithTransformers(rawQuery: ParsedQuery|undefined|null, transformers: IQueryTransformers = {}) {
  const query: ITransformedQuery = {};

  if (rawQuery) {
    for (const key of Object.keys(rawQuery)) {
      const transformer = transformers[key];
      if (transformer) {
        query[key] = transformer.parse(rawQuery[key]);
      } else {
        query[key] = rawQuery[key];
      }
    }
  }

  return query;
}

/**
 * Сериализует квери-параметры
 *
 * @param query - значения квери-параметров
 * @param transformers - набор трансформеров
 * @return объект, который можно передать в stringify из query-string
 */
export function stringifyQueryWithTransformers(query: ITransformedQuery|undefined|null, transformers: IQueryTransformers = {}) {
  const rawQuery: StringifiableRecord = {};

  if (query) {
    for (const key of Object.keys(query)) {
      const transformer = transformers[key];
      if (transformer) {
        rawQuery[key] = transformer.stringify(query[key]);
      } else {
        rawQuery[key] = query[key];
      }
    }
  }

  return rawQuery;
}

/**
 * Возвращает переданное значение, но шаманит с типами
 * чтобы тип результата был наиболее точен,
 * но TS проверял, что он расширяет IQueryTransformers
 *
 * @param x - значение
 */
export function defineQueryTransformers<T extends IQueryTransformers>(x: T) {
  return x;
}

/**
 * Достаёт типы значений из типов трансформеров
 */
export type ExtractTransformedQueryParams<Type extends IQueryTransformers> = {
  [key in keyof Type]: Type[key] extends IQueryTransformer<infer T> ? T : unknown;
}
