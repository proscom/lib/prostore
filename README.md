# Монорепозиторий Prostore

Библиотека для работы с данными во фронтенде.

Prostore - это обертка вокруг RxJS, позволяющая создавать и использовать реактивные сторы для разработки
фронтендовых приложений.

Разработано в [Proscom](https://proscom.ru)

## Документация по отдельным пакетам:

[prostore](./packages/prostore/README.md)

[prostore-apollo](./packages/prostore-apollo/README.md)

[prostore-axios](./packages/prostore-axios/README.md)

[prostore-react](./packages/prostore-react/README.md)

[prostore-apollo-react](./packages/prostore-apollo-react/README.md)

[prostore-axios-react](./packages/prostore-axios-react/README.md)

[prostore-local-storage](./packages/prostore-local-storage/README.md)

[prostore-react-router](./packages/prostore-react-router/README.md)

## С чего начать?

Пока Prostore полноценно поддерживается только в React-приложениях.

Если хотите использовать Prostore для работы с GraphQL API, то начните с документации
[prostore-apollo-react](./packages/prostore-apollo-react/README.md).

Если для работы с REST API, то [prostore-axios-react](./packages/prostore-apollo-react/README.md).

Если хотите понять, как Prostore выглядит в реальном приложении, то см. шаблон фронтенд-проекта в Proscom:
[https://gitlab.com/proscom/front-template](https://gitlab.com/proscom/front-template)

Рецепты использования и общую информацию о том, как устроен Prostore можно получить из документации к основному пакету
[prostore](./packages/prostore/README.md).
